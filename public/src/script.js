import { randomColor, randomNumber } from "./util.js";

const canvas = document.getElementById("Canvas");
const ctx = canvas.getContext("2d");

const canvasWidth = canvas.width;
const canvasHeight = canvas.height;

const tilledSize = Number(localStorage.getItem("DebugTilledSize"));
const rows = [];
const columns = [];
const trail = [];

rows.length = parseInt(canvasWidth / tilledSize);
columns.length = parseInt(canvasHeight / tilledSize);

// Player
let playerX = parseInt(rows.length / 2) * tilledSize;
let playerY = parseInt(columns.length / 2) * tilledSize;
let playerLatsPosition = null;

// Call Stack
let callStack = 0; // for the program don't break
let maxCallStack = 1000; // for the program don't break

// Timer
const millisecondsTimer = Number(localStorage.getItem("DebugTime"));
let moveTimer = null;

const checkValidMove = localStorage.getItem("DebugValidation") === "true";
let runningGame = true; // Stop because got stuck

main();
function main() {
	draw();
	moveTimer = setInterval(makeAMove, millisecondsTimer);
}

function draw() {
	// Clear
	ctx.clearRect(0, 0, canvasWidth, canvasHeight);

	if (tilledSize > 10) {
		for (let i = 0; i < rows.length; i++) {
			for (let j = 0; j < columns.length; j++) {
				ctx.strokeStyle = "rgb(255, 255, 255, 0.5)";
				ctx.strokeRect(i * tilledSize, j * tilledSize, tilledSize, tilledSize);
			}
		}
	}

	if (playerLatsPosition) addNewCell();

	// Path / Trail
	for (let i = trail.length; i > 0; i--) {
		drawSnakePart(trail[i - 1]);
	}

	// Draw Head
	ctx.fillStyle = "white";
	ctx.fillRect(playerX, playerY, tilledSize, tilledSize);
}

function addNewCell() {
	const newCell = {
		x: playerLatsPosition.x,
		y: playerLatsPosition.y,
		color: randomColor(),
	};
	trail.unshift(newCell);
}

function drawSnakePart(cell) {
	// Background
	ctx.fillStyle = cell.color;
	ctx.fillRect(cell.x, cell.y, tilledSize, tilledSize);

	if (tilledSize > 10) {// Border
		ctx.strokestyle = "red";
		ctx.strokeRect(cell.x, cell.y, tilledSize, tilledSize);
	}
}

function makeAMove() {
	if (!runningGame) {
		clearInterval(moveTimer);
		if (confirm("O jogo acabou, Recomeçar?")) location.reload();;
		return;
	}

	const e = randomNumber(1, 4);
	moverPlayer(e);
}

function moverPlayer(keyCode) {
	callStack++;
	playerLatsPosition = { x: playerX, y: playerY };

	if (callStack > maxCallStack) runningGame = false;

	if (keyCode === 1 && playerX > tilledSize - tilledSize) playerX -= tilledSize; // Left
	else if (keyCode === 2 && playerX < canvasWidth - tilledSize) playerX += tilledSize; // Right
	else if (keyCode === 3 && playerY > tilledSize - tilledSize) playerY -= tilledSize; // Up
	else if (keyCode === 4 && playerY < canvasHeight - tilledSize) playerY += tilledSize; // Down
	else makeAMove();

	if (checkValidMove) checkMove();

	draw();
}

function checkMove() {
	let pathClear = true;
	trail.map(c => {
		if (c.x === playerX && c.y === playerY) {
			pathClear = false;
			return;
		}
	});

	if (!pathClear) {
		playerX = playerLatsPosition.x;
		playerY = playerLatsPosition.y;
		makeAMove();
	}
}
