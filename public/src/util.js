export const randomNumber = (min = 0, max = 100) => Math.floor(Math.random() * (max - min + 1) + min);
export const randomColor = () => "#" + (Math.random() * 0xFFFFFF << 0).toString(16);
