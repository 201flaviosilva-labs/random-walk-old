const DebugTime = document.getElementById("DebugTime");
const DebugTilledSize = document.getElementById("DebugTilledSize");
const DebugValidation = document.getElementById("DebugValidation");

DebugTime.addEventListener("change", () => localStorage.setItem("DebugTime", DebugTime.value));
DebugTilledSize.addEventListener("change", () => localStorage.setItem("DebugTilledSize", DebugTilledSize.value));
DebugValidation.addEventListener("change", () => localStorage.setItem("DebugValidation", DebugValidation.checked));

document.getElementById("DebugReloadPage").addEventListener("click", () => location.reload());

// Initialize LocalStorage
// DebugTime
if (!localStorage.getItem("DebugTime")) localStorage.setItem("DebugTime", 500);
else {
	const data = localStorage.getItem("DebugTime");
	DebugTime.value = data;
	document.getElementById("DebugTimeOutput").innerHTML = data;
}

if (!localStorage.getItem("DebugTilledSize")) localStorage.setItem("DebugTilledSize", 50);
else {
	const data = localStorage.getItem("DebugTilledSize");
	DebugTilledSize.value = data;
	document.getElementById("DebugTilledSizeOutput").innerHTML = data;
}

if (!localStorage.getItem("DebugValidation")) localStorage.setItem("DebugValidation", true);
else DebugValidation.checked = localStorage.getItem("DebugValidation") === "true";
